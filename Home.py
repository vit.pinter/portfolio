import streamlit as st
import pandas

st.set_page_config(layout="wide")

col1, col2 = st.columns(2)

with col1:
    st.image("images/photo.jpeg",width=300)

with col2:
    st.title("Vít Pintér")
    content= """ 
    Hello, my name is Vít Pintér. I am a freelance Python programmer, IT Support specialist, Linux enthusiast and polyglot. In the past, I have worked in various companies as an ERP Engineer, web-developer or IT support. I have lived and studied in Japan for half a year.
My interests include Japan, Asia, foreign languages and linguistics, travel, reading, technology, cooking and aquariums.
    """
    st.info(content)

st.write("Bellow you can find some of the apps I have build. Feel free to contact me!")

col3, colSpace, col4 = st.columns([1.5, 0.5, 1.5])

df = pandas.read_csv("data.csv", sep=';')

with col3:
    for i, row in df[:10].iterrows():
        st.header(row['title'])
        st.write(row['description'])
        st.image("images/"+row['image'])
        st.write(f"[Source Code]({row['url']})")

with col4:
    for i, row in df[10:].iterrows():
        st.header(row['title'])
        st.write(row['description'])
        st.image("images/"+row['image'])
        st.write(f"[Source Code]({row['url']})")