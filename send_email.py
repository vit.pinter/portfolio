import smtplib, ssl, os

def sendMail(message):
    host = "smtp.gmail.com"
    port = 465
    password = os.getenv("PASSWORD")
    username = "vit.pinter@gmail.com"
    receiver = username
    context = ssl.create_default_context()

    with smtplib.SMTP_SSL(host, port, context=context) as server:
        server.login(username, password)
        server.sendmail(username, receiver, message)




