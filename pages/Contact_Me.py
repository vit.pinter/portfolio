import streamlit as st
from send_email import *

st.header("Contact Me")

with st.form(key="contactForm"):
    userEmail = st.text_input("Your email address")
    message = st.text_area("Your message")
    button = st.form_submit_button("Submit")
    if button:
        message = f"""\
Subject:  Message from contact form

From: {userEmail}
{message}
"""
        sendMail(message)
        st.info("Your message was sent successfully")

